SUMMARY = "Part of Jalopy Swarm."
DESCRIPTION = "Jalopy client is main application used in Jalopy Swarm project."
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=444903ec679405c557e88ed15ceb2d49"


SRC_URI = "git://git@gitlab.com/ppavacic/jalopy-swarm-client.git;protocol=ssh;branch=feature/ir-communication"
SRCREV = "${AUTOREV}"
PR = "r1"
PV = "+git${SRCPV}"
S = "${WORKDIR}/git"

# build time dependency and  run time dependencies
DEPENDS += "openssl libcryptopp libprotobuf pigpio"		
RDEPENDS_${PN} += "openssl libcryptopp libprotobuf pigpio"

inherit pkgconfig meson

