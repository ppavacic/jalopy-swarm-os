SUMMARY = "Used for IR communication, includes daemon and library"
DESCRIPTION = "pigpio is a library for the Raspberry which allows control of the General Purpose Input Outputs (GPIO)"
SECTION = "devtools"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://UNLICENSED"
VERSION = "v78"
SRC_URI = "git://github.com/joan2937/pigpio.git;protocol=https;tag=${VERSION}"
SOVERSION = "1"
PR = "r1"
PV = "+git${SRCPV}"
S = "${WORKDIR}/git"
TARGET_CC_ARCH += "${LDFLAGS}"


# have to overwrite Makefile from git with local one because CC gets overwritten
do_configure() {
    cp "${THISDIR}/makefiles/libpigpio-Makefile"  ${S}/Makefile
}

do_compile() {
    make
}


do_install() {


    install -d ${D}${libdir}
    install -d ${D}${includedir}
    install -d ${D}${bindir}
    install -d ${D}${mandir}/man1
    install -d ${D}${mandir}/man3
    install -d ${D}${libdir}/pkgconfig
    install -m 0755 -d ${D}/opt/pigpio/cgi
    
    install -m 0644 ${S}/libpigpio.so.${SOVERSION}      ${D}${libdir}/
    install -m 0644 ${S}/libpigpiod_if.so.${SOVERSION}  ${D}${libdir}/
    install -m 0644 ${S}/libpigpiod_if2.so.${SOVERSION} ${D}${libdir}/
    
    lnr ${D}${libdir}/libpigpio.so.${SOVERSION}         ${D}${libdir}/libpigpio.so
    lnr ${D}${libdir}/libpigpiod_if.so.${SOVERSION}     ${D}${libdir}/libpigpiod_if.so
    lnr ${D}${libdir}/libpigpiod_if2.so.${SOVERSION}    ${D}${libdir}/libpigpiod_if2.so
    
	install -m 0755 ${S}/pig2vcd    ${D}${bindir}
	install -m 0755 ${S}/pigpiod    ${D}${bindir}
	install -m 0755 ${S}/pigs       ${D}${bindir}


	install -m 0644 ${S}/pigpio.h                       ${D}${includedir}
	install -m 0644 ${S}/pigpiod_if.h                   ${D}${includedir}
	install -m 0644 ${S}/pigpiod_if2.h                  ${D}${includedir}

	python3 setup.py install --root=${D}/
	
	install -m 0644 ${S}/p*.1                           ${D}${mandir}/man1
	install -m 0644 ${S}/p*.3                           ${D}${mandir}/man3

    
    echo -e "prefix=/usr
exec_prefix=/usr
libdir=${libdir}/
Name: libpigpio
Description: GPIO rpi library
Version: ${VERSION}
URL: https://abyz.me.uk/rpi/pigpio/
Libs: -lpigpio" > ${D}${libdir}/pkgconfig/pigpio.pc
}
FILES_${PN} += "/opt/pigpio/cgi"
FILES_${PN} += "${mandir}/man1 ${mandir}/man3"
FILES_${PN} += "${libdir}/pkgconfig"
FILES_${PN} += "${libdir}/libpigpio*"
FILES_${PN}-dev += "${includedir}"
FILES_${PN} += "/usr/local/lib/python3.6"
