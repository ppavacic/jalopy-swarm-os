SUMMARY = "Language-neutral, platform-neutral extensible mechanism for serializing structured data."
DESCRIPTION = "Protocol buffers are Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data \
– think XML, but smaller, faster, and simpler. You define how you want your data to be structured once, then you can use special generated source \
code to easily write and read your structured data to and from a variety of data streams and using a variety of languages."
HOMEPAGE = "https://developers.google.com/protocol-buffers"
SECTION = "libs"
SRC_URI = "git://github.com/protocolbuffers/protobuf;protocol=git;"

SRCREV = "v3.14.0"

PR = "r1"
PV = "+git${SRCPV}"
S = "${WORKDIR}/git"

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://LICENSE;md5=37b5762e07f0af8c74ce80a8bda4266b"

do_configure_prepend() {
	cd ${S}
    git submodule update --init --recursive
    ./autogen.sh
    cd -
}

inherit pkgconfig autotools
