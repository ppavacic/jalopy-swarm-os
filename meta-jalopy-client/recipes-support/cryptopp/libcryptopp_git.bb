SUMMARY = "Cryptography library"
DESCRIPTION = "Crypto++ Library is a free C++ class library of cryptographic schemes."
HOMEPAGE = "https://www.cryptopp.com/"
SECTION = "libs"
SRC_URI = "git://github.com/weidai11/cryptopp;protocol=git;"

SRCREV = "CRYPTOPP_8_5_0"
CRYPTOPP_VER="8.5.0"

PR = "r1"
PV = "+git${SRCPV}"
S = "${WORKDIR}/git"

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://License.txt;md5=15f12037d9859d059c3a557798163450"

inherit pkgconfig

do_compile() {
    make -j16 static dynamic cryptest.exe
}

do_install() {
    install -d ${D}${libdir}/
    install -d ${D}${includedir}/cryptopp
    
    install -m 0644 ${S}/libcryptopp.so.${CRYPTOPP_VER} ${D}${libdir}/libcryptopp.so.${CRYPTOPP_VER}
    lnr ${D}${libdir}/libcryptopp.so.${CRYPTOPP_VER} ${D}${libdir}/libcryptopp.so
    install -m 0644 ${S}/*.h ${D}${includedir}/cryptopp/
    
    install -d ${D}${libdir}/pkgconfig
    echo -e "prefix=/usr
exec_prefix=/usr
libdir=${libdir}/
Name: Crypto++
Description: A free C++ class library of cryptographic schemes
Version: ${CRYPTOPP_VER}
URL: http://www.cryptopp.com
Libs: -lcryptopp" > ${D}${libdir}/pkgconfig/cryptopp.pc

}

FILES_${PN} += "${libdir}/libcryptopp.so.${CRYPTOPP_VER}"
FILES_${PN} += "${libdir}/libcryptopp.so"
FILES_${PN} += "${libdir}/pkgconfig/"
FILES_${PN}-dev += "${includedir}/cryptopp/*"


