DESCRIPTION = "Recipe for building jalopy-os"
LICENSE = "LGPLv3"

inherit core-image

IMAGE_INSTALL_append = " vim grep python3 "

IMAGE_FEATURES += " splash "
IMAGE_INSTALL_append = " vim grep "

do_image_prepend() {
    bb.warn("Using 'jalopy-image-basic'")
}
