DESCRIPTION = "Recipe for building jalopy-os"
LICENSE = "LGPLv3"

inherit core-image
CORE_IMAGE_EXTRA_INSTALL += "openssh "
DISTRO_FEATURES += "wifi"
IMAGE_INSTALL_append = "sudo grep v4l-utils ir-keytable python3 lsof pkgconfig raspi-gpio dhcp-client init-ifupdown configure-system jalopy-client" 
# jalopy-client removed from -debug image so that small changes don't prevent building OS

