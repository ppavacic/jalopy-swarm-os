FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI_append = " file://interfaces.jalopy "

do_install_append() {
    install -d ${D}${sysconfdir}/network
    install -m 0644 ${WORKDIR}/interfaces.jalopy  ${D}${sysconfdir}/network/interfaces
}

