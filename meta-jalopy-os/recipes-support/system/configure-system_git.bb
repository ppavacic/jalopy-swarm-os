DESCRIPTION = "Configuring system, currently only creates systemd file for setting up wlan0"
LICENSE = "CLOSED"

SRC_URI_append = " file://jalopy-network-init.service "
inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = " jalopy-network-init.service "

# TODO: probably not needed
do_install_append() {
    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/jalopy-network-init.service ${D}/${systemd_unitdir}/system/jalopy-network-init.service
}


