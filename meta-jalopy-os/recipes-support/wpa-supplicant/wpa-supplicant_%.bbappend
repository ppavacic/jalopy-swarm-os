FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI_append = " file://wpa_supplicant.conf.jalopy "

do_install_append() {
    install -m 0644 ${WORKDIR}/wpa_supplicant.conf.jalopy  ${D}${sysconfdir}/wpa_supplicant.conf
}

